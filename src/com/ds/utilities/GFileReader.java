package com.ds.utilities;

import com.ds.dcintegrationtest.Main;
import com.ds.helpers.Helpers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.*;
import java.util.function.Function;
import java.util.logging.Logger;

public class GFileReader<T> {

    public List<T> Read(String filePath, Function<String[], T> entityFactory){

        List<T> entities = new ArrayList<T>();
        int lineNumber = 2;
        Helpers.lineNumber = lineNumber;

        try {

            Main.logger.Log(String.format("Processing file: %s", filePath), false);

            BufferedReader br = new BufferedReader(new FileReader(filePath));

          //  Scanner fileScanner = new Scanner(file);
            String[] row = null;
            String line = "";
            br.readLine();

            while ((line = br.readLine()) != null) {
                //row = (line + ","+ Integer.toString(lineNumber)) .replace("\"","").split(",");

                row = line.trim().replace("\"","").split(",");
                T entity = entityFactory.apply(row);
                entities.add(entity);
                lineNumber++;
                Helpers.lineNumber = lineNumber;
            }

        } catch (Exception e) {
            Main.logger.Log(String.format("Error in line: %d. --> %s",lineNumber, e ) ,true);
        }

        return entities;
    }
}
