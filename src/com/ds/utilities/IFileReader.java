package com.ds.utilities;

import java.util.List;

public interface IFileReader<T> {
    List<T> Read(String filepath, T entityFactory);
}
