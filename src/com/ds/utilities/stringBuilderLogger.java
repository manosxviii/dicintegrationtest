package com.ds.utilities;

import com.ds.dcintegrationtest.Main;

import java.util.Date;

public class stringBuilderLogger implements ILogger {

    public void Log(String message, Boolean invalidData) {

        String _message = String.format("%tT --> %s", new Date(), message);
        Main.log.append(_message);
        Main.log.append("\n");
        System.out.println(_message);

        if (invalidData){
            Main.validation = false;
        }

    }

}
