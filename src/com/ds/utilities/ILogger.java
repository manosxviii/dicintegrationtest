package com.ds.utilities;

public interface ILogger {
    void Log(String message, Boolean invalidData);
}
