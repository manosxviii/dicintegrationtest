package com.ds.dcintegrationtest;

import com.ds.helpers.Resources;
import com.ds.models.customer;
import com.ds.models.invoice;
import com.ds.models.invoiceItem;
import com.ds.utilities.GFileReader;
import com.ds.utilities.ILogger;
import com.ds.utilities.stringBuilderLogger;
import com.sun.deploy.util.StringUtils;

import java.awt.*;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.List;

public class Main {

    public static boolean validation = true;
    public static StringBuilder log = new StringBuilder();
    public static ILogger logger = new stringBuilderLogger();
    private static boolean pathIsValid;

    public static void main(String[] args) {

        String directory = "";

        Scanner reader = new Scanner(System.in);
        while (!pathIsValid) {
            pathIsValid = true;
            System.out.println("Please insert the path of the input files");
            final String path = reader.next().trim();

            Resources.importFiles.forEach(f -> {
                String p = path + f;
                File file = new File(p);

                if (!file.exists() || file.isDirectory()) {
                    System.out.println("Path does not contain file " + p);
                    pathIsValid = false;
                }
            });

            directory = path;
        }
        try {
            GFileReader<customer> customerReader = new GFileReader<customer>();
            List<customer> customers = customerReader.Read(directory + "CUSTOMER.CSV", row -> new customer(row, false));
            GFileReader<invoice> invoiceReader = new GFileReader<invoice>();
            List<invoice> invoices = invoiceReader.Read(directory + "INVOICE.CSV", row -> new invoice(row));
            GFileReader<invoiceItem> invoiceItemReader = new GFileReader<invoiceItem>();
            List<invoiceItem> invoiceItems = invoiceItemReader.Read(directory + "INVOICE_ITEM.CSV", row -> new invoiceItem(row));
            GFileReader<customer> customerSampleReader = new GFileReader<customer>();
            List<customer> customerSamples = customerReader.Read(directory + "CUSTOMER_SAMPLE.CSV", row -> new customer(row, true));

            if (validation) {

                File directoryPath = new File(directory + "\\output\\");
                if (!directoryPath.exists()) {
                    directoryPath.mkdir();
                }

                List<String> lines = new ArrayList<String>();
                lines.add("\"CUSTOMER_CODE\",\"FIRSTNAME\",\"LASTNAME\"");
//
                Comparator<customer> compCustomer = new Comparator<customer>() {
                    public int compare(customer c1, customer c2) {
                        return c1.getCustomerCode().compareTo(c2.getCustomerCode());
                    }
                };

                logger.Log("Extracting Customers...", false);

                Collections.sort(customerSamples, compCustomer);
                customers.forEach(c -> {
                    int i = Collections.binarySearch(customerSamples,
                            new customer(c.getCustomerCode()),
                            compCustomer);
                    if (i >= 0) {
                        String line = c.getCustomerProperties();
                        lines.add(line);
                    }
                });
                Files.write(Paths.get(directory + "\\output\\CUSTOMER.CSV"), lines, StandardCharsets.UTF_8,
                        StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);

                lines.clear();
                lines.add("\"CUSTOMER_CODE\",\"INVOICE_CODE\",\"AMOUNT\",\"DATE\"");



                List<invoiceItem> invoiceItemsSample = new ArrayList<invoiceItem>();

                logger.Log("Extracting Invoices...", false);

                invoices.forEach(inv -> {
                    int i = Collections.binarySearch(customerSamples,
                            new customer(inv.getCustomerCode()),
                            compCustomer);
                    if (i >= 0) {
                        invoiceItemsSample.add(new invoiceItem(inv.getInvoiceCode()));
                        String line = inv.getInvoiceProperties();
                        lines.add(line);
                    }
                });

                Files.write(Paths.get(directory + "\\output\\INVOICE.CSV"), lines, StandardCharsets.UTF_8,
                        StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);

                Comparator<invoiceItem> compInvoice = new Comparator<invoiceItem>() {
                    public int compare(invoiceItem i1, invoiceItem i2) {
                        return i1.getInvoiceCode().compareTo(i2.getInvoiceCode());
                    }
                };

                Collections.sort(invoiceItemsSample, compInvoice);

                logger.Log("Extracting Invoices Items...", false);

                lines.clear();
                lines.add("\"INVOICE_CODE\",\"ITEM_CODE\",\"AMOUNT\",\"QUANTITY\"");

                invoiceItems.forEach(inv -> {
                    int i = Collections.binarySearch(invoiceItemsSample,
                            new invoiceItem(inv.getInvoiceCode()),
                            compInvoice);
                    if (i >= 0) {
                        String line = inv.getInvoiceItemProperties();
                        lines.add(line);
                    }
                });

                Files.write(Paths.get(directory + "\\output\\INVOICE_ITEM.CSV"), lines, StandardCharsets.UTF_8,
                        StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);

                logger.Log("Operation completed successfully", false);

                Desktop.getDesktop().open(new File(directory + "\\output\\"));



            } else {
                logger.Log("Incorrect input files. Application will terminate.", true);
            }


        } catch (Exception e) {
            logger.Log(e.getMessage(), true);
        }


    }
}



