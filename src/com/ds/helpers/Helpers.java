package com.ds.helpers;

import com.ds.dcintegrationtest.Main;

import java.io.IOException;
import java.text.*;
import java.io.File;
import java.util.Date;
import java.util.Locale;

public class Helpers {

    public static int lineNumber = 0;

    //checks if path contains the needed files
    public static void PathCheck(String path) {


    }

    public static Date parseDate(String input) throws ParseException {

        Date result;
        try {
          DateFormat df = new SimpleDateFormat("dd-MMM-yyyy",Locale.ENGLISH);

            result = df.parse(input);
            return result;

        } catch (Exception e) {
            Main.logger.Log(String.format("Error in line: %d. --> %s",lineNumber, e ),true);
            return null;
        }
//        finally {
//           // return result;
//        }
    }

    public static String dateToString(Date input){
        DateFormat df = new SimpleDateFormat("dd-MMM-yyyy",Locale.ENGLISH);
        return df.format(input);
    }

    public static Double parseDouble(String input) {
        Double result = 0.0;

        try{
            result = Double.parseDouble(input);
           // return result;
        }
        catch (Exception e){
            Main.logger.Log(String.format("Error in line: %d. --> %s",lineNumber, e ),true);
        }
        finally {
            return result;
        }
    }


    public static Integer parseInteger(String input){
        Integer result = 0;

        try{
            result = Integer.parseInt(input);
            // return result;
        }
        catch (Exception e){
            Main.logger.Log(String.format("Error in line: %d. --> %s",lineNumber, e ),true);
        }
        finally {
            return result;
        }
    }

    public static String getStringDouble(Double input){
        NumberFormat formatter = new DecimalFormat("#0.00");
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
        DecimalFormat df = new DecimalFormat("#0.00", otherSymbols);
        return df.format(input);
    }


//    public static void createNewFile(File file) throws IOException {
//        if (file.createNewFile()){
//            System.out.println("File is created!");
//        }else{
//            System.out.println("File already exists.");
//        }
//    }

}
