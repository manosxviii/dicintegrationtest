package com.ds.models;

import com.ds.helpers.Helpers;

public class invoiceItem {

    private String InvoiceCode;

    public void setInvoiceCode(String invoiceCode) {
        InvoiceCode = invoiceCode;
    }

    public String getInvoiceCode() {
        return InvoiceCode;
    }

    private String ItemCode;

    public String getItemCode() {
        return ItemCode;
    }

    public void setItemCode(String itemCode) {
        ItemCode = itemCode;
    }

    private Double Amount;

    public void setAmount(Double amount) {
        Amount = amount;
    }

    public Double getAmount() {
        return Amount;
    }

    private Integer Quantity;

    public Integer getQuantity() {
        return Quantity;
    }

    public void setQuantity(Integer quantity) {
        Quantity = quantity;
    }


    public invoiceItem(String[] rowData) {

        try {
            InvoiceCode = rowData[0];
            ItemCode = rowData[1];
            Amount = Helpers.parseDouble(rowData[2]);//Double.parseDouble(rowData[2]);
            Quantity =  Helpers.parseInteger(rowData[3]);// Integer.parseInt(rowData[3]);

        } catch (Exception e) {
            throw e;
        }
    }

    public invoiceItem(String invCode) {
        InvoiceCode = invCode;
    }

    public String getInvoiceItemProperties(){
        String s = "\"";
        String m = "\",\"";

        return s+InvoiceCode+ m + ItemCode + m + Helpers.getStringDouble(Amount) + m + String.valueOf(Quantity) + s;
    }
}

