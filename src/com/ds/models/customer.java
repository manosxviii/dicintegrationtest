package com.ds.models;

import java.util.Arrays;

public class customer {

    private String CustomerCode;
    public String getCustomerCode() {
        return CustomerCode;
    }
    public void setCustomerCode(String customerCode) {
        CustomerCode = customerCode;
    }

    private String FirstName;
    public String getFirstName() {
        return FirstName;
    }
    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    private String LastName;
    public String getLastName() {
        return LastName;
    }
    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public customer(String[] rowData, Boolean isSample){
        try{
            CustomerCode = rowData[0];
            if(!isSample) {
                FirstName = rowData[1];
                LastName = rowData[2];
            }
        }catch (Exception e){
            throw e;
        }
    }

    public customer(String custCode){
        CustomerCode = custCode;
    }

    public String getCustomerProperties(){
        String s = "\"";
        String m = "\",\"";

        return s+CustomerCode+ m + FirstName + m + LastName + s;
    }
}
