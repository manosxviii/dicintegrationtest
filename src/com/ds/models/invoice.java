package com.ds.models;

import com.ds.dcintegrationtest.Main;
import com.ds.helpers.Helpers;
import sun.util.resources.th.CalendarData_th;

import java.text.ParseException;

public class invoice {
    private String CustomerCode;
    public String getCustomerCode() {
        return CustomerCode;
    }
    public void setCustomerCode(String customerCode) {
        CustomerCode = customerCode;
    }

    private String InvoiceCode;
    public String getInvoiceCode() {
        return InvoiceCode;
    }
    public void setInvoiceCode(String invoiceCode) {
        InvoiceCode = invoiceCode;
    }

    private Double Amount;
    public Double getAmount() {
        return Amount;
    }
    public void setAmount(Double amount) {
        Amount = amount;
    }

    private java.util.Date Date;
    public java.util.Date getDate() {
        return Date;
    }
    public void setDate(java.util.Date date) {
        Date = date;
    }


    public invoice(String[] rowData) {

        try{
            CustomerCode = rowData[0];
            InvoiceCode = rowData[1];
            Amount = Helpers.parseDouble(rowData[2]);//Double.parseDouble(rowData[2]);
            try{Date = Helpers.parseDate(rowData[3]);} catch (Exception e){}
        }
        catch (Exception e){
           throw e;
        }



    }

    public String getInvoiceProperties(){
        String s = "\"";
        String m = "\",\"";

        return s+CustomerCode+ m + InvoiceCode + m + Helpers.getStringDouble(Amount) + m + Helpers.dateToString(Date) + s;
    }
}
